
__all__ = ['SingletonLogger']
__author__ = 'Shrikrashna Kadam'

import os, inspect
from xpresso.ai.core.commons.utils.constants import COMPONENTNAME_KEY, PIPELINE_NAME_KEY, \
    XPRESSO_RUN_NAME, PROJECT_NAME_KEY, XPR_WORKFLOW_KEY, COMPONENT_NAME_ENV, PROJECT_NAME_ENV, \
    PIPELINE_NAME_ENV, XPRESSO_RUN_NAME_ENV, XPR_WORKFLOW_ENV, XPRESSO_LOG_CONTEXT
from xpresso.ai.core.commons.utils.generic_utils import merge_dicts


class XprLoggerContext:
    """
    Logger context which will be set during each __call__ method of xpresso logger
    """

    def __init__(self, **kwargs):
        env_labels = self.get_component_env_labels()
        # Preference is given to the values of parameter passed by the user while initialising
        self.user_logs = os.getenv(XPRESSO_LOG_CONTEXT, 'False').lower() == 'false'
        self.name = kwargs.get(PROJECT_NAME_KEY) if kwargs.get(PROJECT_NAME_KEY) else env_labels.get(PROJECT_NAME_KEY)
        self.config_path = kwargs.get('config_path')
        self.level = kwargs.get('level', None)
        self.component_name = kwargs.get(COMPONENTNAME_KEY) if kwargs.get(COMPONENTNAME_KEY) else env_labels.get(
            COMPONENTNAME_KEY)
        self.pipeline_name = kwargs.get(PIPELINE_NAME_KEY) if kwargs.get(PIPELINE_NAME_KEY) else env_labels.get(
            PIPELINE_NAME_KEY)
        self.xpresso_run_name = kwargs.get(XPRESSO_RUN_NAME) if kwargs.get(XPRESSO_RUN_NAME) else env_labels.get(
            XPRESSO_RUN_NAME)
        self.xpresso_workflow = kwargs.get(XPR_WORKFLOW_KEY) if kwargs.get(XPR_WORKFLOW_KEY) else env_labels.get(XPR_WORKFLOW_ENV)

    @staticmethod
    def get_component_env_labels():
        # Try to get values from the environment variables if not passed
        env_labels = {}
        env_labels.update({COMPONENTNAME_KEY: os.getenv(COMPONENT_NAME_ENV, None)})
        env_labels.update({PROJECT_NAME_KEY: os.getenv(PROJECT_NAME_ENV, None)})
        env_labels.update({PIPELINE_NAME_KEY: os.getenv(PIPELINE_NAME_ENV, None)})
        env_labels.update({XPRESSO_RUN_NAME: os.getenv(XPRESSO_RUN_NAME_ENV, None)})
        env_labels.update({XPR_WORKFLOW_ENV: os.getenv(XPR_WORKFLOW_ENV, None)})
        return env_labels


class SingletonLogger(type):
    """
    Define an Instance of Logger which will be unique instance and will help to manage it's
    own context
    """

    def __init__(cls, name, bases, attrs, **kwargs):
        super().__init__(name, bases, attrs)
        cls._instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)

        # Converting non-parameterised arguments to parameterised
        cframe = inspect.currentframe()
        merge_dicts(kwargs, cls.get_parameterised_args(cframe))

        # Setting up the logger context
        cls._instance.xpresso_logs = os.getenv(XPRESSO_LOG_CONTEXT, 'False').lower() == 'true'
        if cls._instance.xpresso_logs:
            cls._instance.xpresso_logger_context = XprLoggerContext(**kwargs)
        else:
            cls._instance.user_logger_context = XprLoggerContext(**kwargs)

        # calling set_logger to update the configurations
        cls._instance.set_logger_config()

        return cls._instance

    def get_parameterised_args(cls, cframe):
        """
        Converts positional non-parameterised arguments to parameterised
        """
        non_param_args = inspect.getfullargspec(cls.__init__)

        non_param_args_defaults = dict(zip(non_param_args.args[-len(non_param_args.defaults):],
                                       non_param_args.defaults))
        local_args = inspect.getargvalues(cframe).locals.get('args')
        non_param_args_valued_dict = dict(zip(non_param_args.args[1:len(local_args)], local_args[:len(local_args)]))

        non_param_args_default_values = {key: value for (key, value) in non_param_args_defaults.items() if value}
        non_param_args_default_values.update(non_param_args_valued_dict)
        return non_param_args_default_values
